use std::{collections::HashMap, fs::File, io::prelude::Write, process::Command, time::Duration};
use url::Url;

fn main() {
    //Set the time out long enough to download a video.
    let client = reqwest::blocking::Client::builder()
        .timeout(Duration::from_secs(60 * 60))
        .build()
        .unwrap();
    //Get first argument from command line as input either a video link or a video id.
    let input = std::env::args().nth(1).expect("no vid id given");
    let mut video_id = "".to_owned();
    //Check if the the input contains http/https => link else => id.
    if (&input).starts_with("https://") || (&input).starts_with("http://") {
        match Url::parse(&input) {
            Ok(parsed_url) => {
                let hash_query: HashMap<_, _> = parsed_url.query_pairs().into_owned().collect();
                video_id = hash_query.get("v").unwrap().as_str().to_owned();
            }
            Err(e) => eprintln!("{:?}", e),
        }
    } else {
        video_id = input;
    }

    let info_url = format!("https://youtube.com/get_video_info?video_id={}&html5=1", video_id);

    match client.get(&info_url).send().unwrap().text() {
        Ok(v) => {
            let resp_map = serde_urlencoded::from_str::<HashMap<String, String>>(&v).unwrap()
                as HashMap<String, String>; 
            let json_player_resp =
                json::parse(&resp_map.get::<str>(&"player_response").unwrap() as &str).unwrap();
            let streaming_data = &json_player_resp["streamingData"]["formats"];
            let video_details = &json_player_resp["videoDetails"];
            println!("video_details: {}", &video_details["title"].to_string());
            let file_name = format!("{}.mp4", &video_details["title"].to_string());
            let normalized_file_name: String = format!(
                "{}",
                file_name
                    .chars()
                    .map(|x| match x {
                        ':' => '-',
                        '"' => ' ',
                        '/' => ' ',
                        '\\' => ' ',
                        '*' => ' ',
                        '?' => ' ',
                        '<' => ' ',
                        '>' => ' ',
                        '|' => ' ',
                        _ => x,
                    })
                    .collect::<String>()
            );
            println!("normalized_file_name: {}", &normalized_file_name);
            if std::path::Path::new(&normalized_file_name).exists() {
                println!("file exist");
                return;
            }
            let mut file = File::create(&normalized_file_name).unwrap();
            match (&streaming_data)[streaming_data.len() - 1]["url"].as_str() {
                Some(url) => {
                    println!("streaming_data: {:?}", &url);
                    let downloaded_video = client.get(url).send().unwrap().bytes().unwrap();
                    file.write_all(&downloaded_video).unwrap();
                }
                None => {
                    download_ciphered_video(&normalized_file_name, &video_id);
                }
            }
        }
        Err(e) => println!("error parsing header: {:?}", e),
    }
}

// Function to use node package ytdl to download ciphered video link.
fn download_ciphered_video(normalize_file_name: &str, video_id: &str) {
    Command::new("node")
        .arg("node_modules/ytdl/bin/ytdl.js")
        .arg(video_id)
        .arg("-o")
        .arg(normalize_file_name)
        .arg("-q")
        .arg("22")
        .output()
        .expect("failed to execute process");
}
