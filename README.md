# Simple Rust Youtube Downloader 
I am writing a quick prototype of youtube donwloader in Rust for learning purpose. I document step by step to set up project in [a blog](https://www.ninjahoahong.com/2021/04/simple-rust-youtube-downloader.html).

## Libraries used in this projects
* [reqwest (MIT/Apache License, Version 2.0)](https://github.com/seanmonstar/reqwest)
* [rust-url (MIT/Apache License, Version 2.0)](https://github.com/servo/rust-url)
* [json-rust (Apache License, Version 2.0)](https://github.com/maciejhirsz/json-rust)
* [serde_urlencoded (Apache License, Version 2.0)](https://github.com/nox/serde_urlencoded/blob/master/LICENSE-APACHE)
